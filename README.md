# twquery

powerful query language and engine that simplifies the interaction with relational databases

## Details

`twquery` provides a simplified way to construct simple or complex requests to query relational databases. It is used to query the `twquery engine`, which converts twquery into SQL queries and returns the appropriate data. This is the JavaScript library that helps construct twquery.

## Under Construction

We are actively building this package. [`twquery-types`](https://www.npmjs.com/package/twquery-types), which is the Typescript definition for twquery, is already up at [npm](https://www.npmjs.com/package/twquery-types).
